import { Project } from '@prisma/client'
import { Row, Workbook } from 'exceljs'
import { ReadStream } from 'fs'
import { Context } from '../../context'

export interface Meta {
  worksheet: number
  headersRow: number
  firstDataRow: number
  item: {
    nameColumn: string
    volumeColumns: string[]
    specsColumns: string[]
  }
  supplier: {
    nameColumn: string
  }
  offer: {
    priceColumns: string[]
    specsColumns: string[]
  }
}

export const createProjectFromXlsx = async (
  ctx: Context,
  projectName: string,
  userId: string,
  meta: Meta,
  stream: ReadStream,
): Promise<Project | null> => {
  const workbook = new Workbook()
  await workbook.xlsx.read(stream)

  if (workbook.worksheets.length === 0) {
    throw new Error('the xlsx file has no worksheets')
  }

  const worksheet = workbook.getWorksheet(meta.worksheet)
  const headers = worksheet.getRow(meta.headersRow)
  const endRow = worksheet.actualRowCount - meta.firstDataRow + 1
  const rows = worksheet.getRows(meta.firstDataRow, endRow)

  if (!rows) {
    throw new Error(
      `the worksheet has no rows from ${meta.firstDataRow} to ${endRow}`,
    )
  }

  return await ctx.prisma.$transaction(async (prisma) => {
    const user = await prisma.user.findUnique({
      where: { id: userId },
    })

    if (user === null) {
      throw new Error(`user ${userId} not found`)
    }

    const newProject = await prisma.project.create({
      data: {
        name: projectName,
        organizationId: user.organizationId,
        userId: user.id,
      },
    })

    for (const rawRow of rows) {
      const row = parseRow(rawRow, headers, meta)

      const item = await prisma.item.upsert({
        where: {
          itemId: {
            name: row.item.name,
            projectId: newProject.id,
          },
        },
        update: {
          volume: row.item.volume,
          specifications: row.item.specifications,
        },
        create: {
          name: row.item.name,
          volume: row.item.volume,
          specifications: row.item.specifications,
          projectId: newProject.id,
        },
      })

      const supplier = await prisma.supplier.upsert({
        where: {
          supplierId: {
            name: row.supplier.name,
            organizationId: user.organizationId,
          },
        },
        update: {},
        create: {
          name: row.supplier.name,
          organizationId: user.organizationId,
        },
      })

      const offer = await prisma.offer.create({
        data: {
          itemId: item.id,
          supplierId: supplier.id,
          price: row.offer.price,
          specifications: row.offer.specifications,
        },
      })
    }

    return newProject
  })
}

const sumCellValues = (row: Row, columns: string[]) =>
  columns
    .map((column) => {
      const cell = row.getCell(column)
      const float = parseFloat(cell.value as string)
      if (isNaN(float)) {
        throw new Error(`cannot parse cell ${cell.$col$row} as float`)
      }
      return float
    })
    .reduce((result, value) => result + value, 0)

const getSpecs = (
  row: Row,
  headers: Row,
  columns: string[],
): Record<string, string> =>
  columns
    .map((column) => ({
      name: headers.getCell(column).value as string,
      value: row.getCell(column).value,
    }))
    .reduce(
      (result, { name, value }) => ({
        ...result,
        [name]: value,
      }),
      {},
    )

const parseItem = (row: Row, headers: Row, meta: Meta) => ({
  name: row.getCell(meta.item.nameColumn).value as string,
  volume: sumCellValues(row, meta.item.volumeColumns),
  specifications: getSpecs(row, headers, meta.item.specsColumns),
})

const parseSupplier = (row: Row, headers: Row, meta: Meta) => ({
  name: row.getCell(meta.supplier.nameColumn).value as string,
})

const parseOffer = (row: Row, headers: Row, meta: Meta) => ({
  price: sumCellValues(row, meta.offer.priceColumns),
  specifications: getSpecs(row, headers, meta.offer.specsColumns),
})

const parseRow = (row: Row, headers: Row, meta: Meta) => ({
  item: parseItem(row, headers, meta),
  supplier: parseSupplier(row, headers, meta),
  offer: parseOffer(row, headers, meta),
})
