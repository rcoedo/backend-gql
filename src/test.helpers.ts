import http from 'http'
import path from 'path'
import { ApolloServer, ExpressContext } from 'apollo-server-express'
import { execSync } from 'child_process'
import { GraphQLClient } from 'graphql-request'
import { startApolloServer } from './server'
import { AddressInfo } from 'net'
import { PrismaClient } from '@prisma/client'

type TestContext = {
  client: GraphQLClient
  db: PrismaClient
}

export function createTestContext(): TestContext {
  let ctx = {} as TestContext

  const graphqlCtx = graphqlTestContext()
  const prismaCtx = prismaTestContext()

  beforeAll(async () => {
    const client = await graphqlCtx.before()
    const db = await prismaCtx.before()

    Object.assign(ctx, {
      client,
      db,
    })
  })

  afterAll(async () => {
    await graphqlCtx.after()
    await prismaCtx.after()
  })

  return ctx
}

const graphqlTestContext = () => {
  let server: {
    apolloServer: ApolloServer<ExpressContext>
    httpServer: http.Server
  }

  return {
    async before() {
      server = await startApolloServer(0)
      const address = server.httpServer.address() as AddressInfo
      return new GraphQLClient(`http://localhost:${address.port}/graphql`)
    },
    async after() {
      server?.apolloServer.stop()
      server?.httpServer.close()
    },
  }
}

function prismaTestContext() {
  const prismaBinary = path.join(
    __dirname,
    '..',
    'node_modules',
    '.bin',
    'prisma',
  )

  let prismaClient: null | PrismaClient = null

  return {
    async before() {
      // Run the migrations to ensure our schema has the required structure
      execSync(`${prismaBinary} db push`)

      // Construct a new Prisma Client connected to the generated schema
      const client = new PrismaClient()

      const chem = await client.organization.upsert({
        where: { name: 'Chem Ltd' },
        update: {},
        create: {
          id: '15c4b32a-f24c-4efb-ac24-98872b579e8d',
          name: 'Chem Ltd',
        },
      })

      await client.user.upsert({
        where: { name: 'Sarah' },
        update: {},
        create: {
          id: '7824269e-dfe6-4f68-bfcf-2fe7d8c44c4d',
          name: 'Sarah',
          organizationId: chem.id,
        },
      })

      return client
    },
    async after() {
      await prismaClient?.$disconnect()
    },
  }
}
