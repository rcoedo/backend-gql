import { startApolloServer } from './server'
;(async () => {
  const server = await startApolloServer(4000)
  console.log(
    `🚀 Server ready at http://localhost:4000${server.apolloServer.graphqlPath}`,
  )
})()
