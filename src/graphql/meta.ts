import { inputObjectType } from 'nexus'

export const ItemMetaInputType = inputObjectType({
  name: 'ItemMetaType',
  definition(t) {
    t.nonNull.string('nameColumn')
    t.nonNull.list.nonNull.string('volumeColumns')
    t.nonNull.list.nonNull.string('specsColumns')
  },
})

export const SupplierMetaInputType = inputObjectType({
  name: 'SupplierMetaType',
  definition(t) {
    t.nonNull.string('nameColumn')
  },
})

export const OfferMetaInputType = inputObjectType({
  name: 'OfferMetaType',
  definition(t) {
    t.nonNull.list.nonNull.string('priceColumns')
    t.nonNull.list.nonNull.string('specsColumns')
  },
})

export const MetaInputType = inputObjectType({
  name: 'MetaInputType',
  definition(t) {
    t.nonNull.int('worksheet')
    t.nonNull.int('headersRow')
    t.nonNull.int('firstDataRow')
    t.nonNull.field('item', { type: ItemMetaInputType })
    t.nonNull.field('supplier', { type: SupplierMetaInputType })
    t.nonNull.field('offer', { type: OfferMetaInputType })
  },
})
