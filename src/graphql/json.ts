import { scalarType } from 'nexus'

export const JSONScalar = scalarType({
  name: 'JSON',
  asNexusMethod: 'json',
  parseValue(value) {
    return JSON.parse(value)
  },
  serialize(value) {
    return JSON.stringify(value)
  },
})
