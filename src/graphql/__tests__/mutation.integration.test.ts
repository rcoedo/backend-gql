import { createReadStream } from 'fs'
import path from 'path'
import { gql } from 'graphql-request'
import { createTestContext } from '../../test.helpers'

const ctx = createTestContext()

describe('graphql', () => {
  const UploadBidsheet = gql`
    mutation Upload(
      $organizationId: ID!
      $projectName: String!
      $bidsheet: Upload!
      $meta: MetaInputType!
    ) {
      uploadBidsheet(
        organizationId: $organizationId
        projectName: $projectName
        file: $bidsheet
        meta: $meta
      ) {
        id
        name
      }
    }
  `
  describe('uploadBidsheet mutation', () => {
    it('works if the xlsx is parseable', async () => {
      const result = await ctx.client.request(UploadBidsheet, {
        userId: '7824269e-dfe6-4f68-bfcf-2fe7d8c44c4d',
        projectName: 'project-name',
        meta: {
          worksheet: 1,
          headersRow: 2,
          firstDataRow: 3,
          item: {
            nameColumn: 'A',
            volumeColumns: ['B', 'C'],
            specsColumns: ['D', 'E', 'F'],
          },
          supplier: {
            nameColumn: 'G',
          },
          offer: {
            priceColumns: ['K', 'L', 'M'],
            specsColumns: ['O'],
          },
        },
        bidsheet: createReadStream(
          path.join(__dirname, './data/chem_no_errors.xlsx'),
        ),
      })

      expect(result).toMatchInlineSnapshot()
    })

    it('throws an error if a cell is not parseable', async () => {
      expect.assertions(1)

      try {
        await ctx.client.request(UploadBidsheet, {
          userId: '7824269e-dfe6-4f68-bfcf-2fe7d8c44c4d',
          projectName: 'project-name',
          meta: {
            worksheet: 1,
            headersRow: 2,
            firstDataRow: 3,
            item: {
              nameColumn: 'A',
              volumeColumns: ['B', 'C'],
              specsColumns: ['D', 'E', 'F'],
            },
            supplier: {
              nameColumn: 'G',
            },
            offer: {
              priceColumns: ['K', 'L', 'M'],
              specsColumns: ['O'],
            },
          },
          bidsheet: createReadStream(
            path.join(__dirname, './data/chem_with_errors.xlsx'),
          ),
        })
      } catch (error) {
        if (error instanceof Error) {
          expect(error.message).toContain('cannot parse cell $K$23 as float')
        }
      }
    })
  })
})
