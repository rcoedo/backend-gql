import { gql } from 'graphql-request'
import { createTestContext } from '../../test.helpers'

const ctx = createTestContext()

describe('graphql', () => {
  describe('organization query', () => {
    it('returns valid queries', async () => {
      const query = gql`
        {
          organizations {
            id
            name
            projects {
              id
              name
            }
          }
        }
      `
      const result = await ctx.client.request(query)

      expect(result).toMatchInlineSnapshot()
    })
  })
})
