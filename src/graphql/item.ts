import { Offer, Supplier } from '@prisma/client'
import { objectType } from 'nexus'

export const Item = objectType({
  name: 'Item',
  definition(t) {
    t.nonNull.id('id')
    t.nonNull.string('name')
    t.nonNull.float('volume')
    t.json('specifications')
    t.nonNull.list.nonNull.field('offers', {
      type: 'Offer',
      resolve: async (parent, _args, context) => {
        const offers = await context.prisma.offer.findMany({
          where: { itemId: parent.id },
          include: { supplier: true },
        })

        return offers.map((offer: Offer & { supplier: Supplier }) => {
          return {
            ...offer,
            supplier: offer.supplier.name,
          }
        })
      },
    })
  },
})
