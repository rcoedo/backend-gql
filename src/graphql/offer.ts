import { objectType } from 'nexus'

export const Offer = objectType({
  name: 'Offer',
  definition(t) {
    t.nonNull.id('id')
    t.nonNull.float('price')
    t.json('specifications')
    t.nonNull.string('supplier')
  },
})
