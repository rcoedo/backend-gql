import { idArg, mutationField, nonNull, stringArg } from 'nexus'
import { MetaInputType } from './meta'
import { createProjectFromXlsx } from '../model/project/project.service'
import { ApolloError } from 'apollo-server-core'

export const uploadBidsheet = mutationField('uploadBidsheet', {
  type: 'Project',
  args: {
    userId: nonNull(idArg()),
    projectName: nonNull(stringArg()),
    file: nonNull('Upload'),
    meta: nonNull(MetaInputType),
  },
  resolve: async (_parent, args, ctx) => {
    const { createReadStream } = await args.file
    const stream = createReadStream()

    try {
      const project = await createProjectFromXlsx(
        ctx,
        args.projectName,
        args.userId,
        args.meta,
        stream,
      )

      return project
    } catch (error) {
      if (error instanceof Error) {
        throw new ApolloError(error.message)
      }
      throw error
    }
  },
})
