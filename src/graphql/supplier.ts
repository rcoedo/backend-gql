import { objectType } from 'nexus'

export const Supplier = objectType({
  name: 'Supplier',
  definition(t) {
    t.nonNull.id('id')
    t.nonNull.string('name')
  },
})
