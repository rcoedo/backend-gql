import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core'
import { ApolloServer, ExpressContext } from 'apollo-server-express'
import express from 'express'
import { GraphQLSchema } from 'graphql'
import { graphqlUploadExpress } from 'graphql-upload'
import http from 'http'
import { context } from './context'
import { schema } from './schema'

export async function startApolloServer(port = 4000) {
  // Required logic for integrating with Express
  const app = express()
  const httpServer = http.createServer(app)

  app.use(graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 1 }))

  // Same ApolloServer initialization as before, plus the drain plugin.
  const apolloServer = new ApolloServer({
    schema: schema as unknown as GraphQLSchema,
    context,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  })

  // More required logic for integrating with Express
  await apolloServer.start()
  apolloServer.applyMiddleware({
    app,
  })

  return new Promise<{
    apolloServer: ApolloServer<ExpressContext>
    httpServer: http.Server
  }>((resolve) =>
    httpServer.listen({ port }, () => resolve({ httpServer, apolloServer })),
  )
}
