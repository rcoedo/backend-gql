import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

async function main() {
  const bor = await prisma.organization.upsert({
    where: { name: 'Bor Corp' },
    update: {},
    create: {
      name: 'Bor Corp',
    },
  })

  const chem = await prisma.organization.upsert({
    where: { name: 'Chem Ltd' },
    update: {},
    create: {
      name: 'Chem Ltd',
    },
  })

  await prisma.user.upsert({
    where: { name: 'Zack' },
    update: {},
    create: {
      name: 'Zack',
      organizationId: bor.id,
    },
  })

  await prisma.user.upsert({
    where: { name: 'Sarah' },
    update: {},
    create: {
      name: 'Sarah',
      organizationId: chem.id,
    },
  })
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
